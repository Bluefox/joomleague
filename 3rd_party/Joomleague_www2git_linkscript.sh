#!/bin/bash

GIT=/home/And_One/git/jsports/joomleague
WWW=/var/www/joomleague

clear

#####################
# Standard Component
#####################
echo remove components/com_joomleague
rm -rf $WWW/components/com_joomleague
echo link components/com_joomleague with GIT
ln -s $GIT/com_joomleague/components/com_joomleague/ $WWW/components/
      
echo remove administrator/components/com_joomleague
rm -rf $WWW/administrator/components/com_joomleague
echo link administrator/components/com_joomleague with GIT
ln -s $GIT/com_joomleague/administrator/components/com_joomleague/ $WWW/administrator/components/com_joomleague
      
echo remove media/com_joomleague
rm -rf $WWW/media/com_joomleague
echo link media/com_joomleague with GIT
ln -s $GIT/com_joomleague/media/com_joomleague/ $WWW/media/

#language en-GB
echo remove language/en-GB/en-GB.com_joomleague.ini
rm $WWW/language/en-GB/en-GB.com_joomleague.ini
echo link com_joomleague/language/en-GB/en-GB.com_joomleague.ini with GIT 
ln $GIT/com_joomleague/language/en-GB/en-GB.com_joomleague.ini $WWW/language/en-GB

echo remove administrator/language/en-GB/en-GB.com_joomleague.ini
rm $WWW/administrator/language/en-GB/en-GB.com_joomleague.ini
echo link com_joomleague/administrator/language/en-GB/en-GB.com_joomleague.ini with GIT
ln $GIT/com_joomleague/administrator/language/en-GB/en-GB.com_joomleague.ini $WWW/administrator/language/en-GB

##################
# Additional Files 
##################
# language de-DE
echo remove language file administrator/language/de-DE/de-DE.com_joomleague.ini
rm -rf $WWW/administrator/language/de-DE/de-DE.com_joomleague.ini
echo link administrator/language/de-DE/de-DE.com_joomleague.ini with GIT
ln $GIT/languages/component/de-DE/admin/de-DE.com_joomleague.ini $WWW/administrator/language/de-DE

echo remove administrator/language/de-DE/de-DE.com_joomleague_menu.ini
rm -rf $WWW/administrator/language/de-DE/de-DE.com_joomleague_menu.ini
echo link administrator/language/de-DE/de-DE.com_joomleague_menu.ini with GIT
ln $GIT/languages/component/de-DE/admin/de-DE.com_joomleague_menu.ini $WWW/administrator/language/de-DE
                                                                      
echo remove language/de-DE/de-DE.com_joomleague.ini
rm -rf $WWW/language/de-DE/de-DE.com_joomleague.ini
echo link language/de-DE/de-DE.com_joomleague.ini with GIT
ln $GIT/languages/component/de-DE/site/de-DE.com_joomleague.ini $WWW/language/de-DE
 